﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CharacterController : MonoBehaviour {

	public float maxDiapasaoTime = 0.5f;
	private float t;
	public Transform diapasaoWave;
	public float maxDiapasaoRadius= 5;
	public Transform diapasao;
	public GvrAudioSource diapasaoAS;
	public GvrAudioSource beanAS;
	public Transform playerFace;

	public static CharacterController instance;

	void Awake() {
		instance = this;
	}

	void Start() {
		GameManager.instance.PlayNextLevel ();
	}

	// Update is called once per frame
	void Update () {
		if (GameManager.instance.isGameOver) {
			return;
		}
		CheckDiapasao ();
	}

	private void CheckDiapasao() {
		t += Time.deltaTime;
		if (t >= maxDiapasaoTime * 2) {
			t = 0;
			StartCoroutine(PlayDiapasaoWave ());
		}
	}

	private IEnumerator PlayDiapasaoWave() {
		diapasao.DOShakePosition (maxDiapasaoTime, 0.01f, 10);

		diapasaoAS.Play ();

		diapasaoWave.localScale = Vector3.zero;
		var t = 0f;
		while (t < maxDiapasaoTime) {
			t+= Time.deltaTime;
			diapasaoWave.localScale = Vector3.one * Mathf.Lerp (0, maxDiapasaoRadius, t);
			diapasaoWave.GetComponent<MeshRenderer> ().material.SetFloat ("_BumpAmt", Mathf.Lerp (20,0,t));
			yield return null;
		}

		diapasaoWave.GetComponent<MeshRenderer> ().material.SetFloat ("_BumpAmt", 0);

		yield return new WaitForSeconds (maxDiapasaoTime);
	}
}
