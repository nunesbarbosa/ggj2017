﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NiobiumStudios;
using System;

public class GhostSpawner : UnitySingleton<GhostSpawner> {

	public float spawnRadius = 15f;
	public int enemiesInLevel;
	public PoolController.PoolConfiguration[] ghostsPrefabs;
	public List<LevelConfiguration> levelConfigurations = new List<LevelConfiguration>();

	public delegate void OnLevelEnd();
	public static OnLevelEnd onLevelEnd;

	void Start()
	{
		InitializePool();
	}

	private void InitializePool()
	{
		PoolController.instance.InitializePool(ghostsPrefabs);
	}

	private GameObject SpawnGhost(int ghostIdx) {
		var go =  PoolController.instance.GetFromPool(ghostsPrefabs[ghostIdx].GetID());
		var randomCircle = (UnityEngine.Random.insideUnitCircle * spawnRadius);
		var randomHeight = UnityEngine.Random.Range (0.5f, 1f);

		if (randomCircle.x > 0) {
			randomCircle.x += 5f;
		} else {
			randomCircle.x -= 5f;
		}

		if (randomCircle.y > 0) {
			randomCircle.y += 5f;
		} else {
			randomCircle.y -= 5f;
		}
		go.transform.position = new Vector3 (randomCircle.x, randomHeight, randomCircle.y);

		go.GetComponent<GhostController> ().Reset ();
		go.SetActive (true);

		return go;
	}

	[Serializable]
	public class LevelConfiguration {
		public int level;
		public List<int> ghostIdxs;
		public float delayBetweenSpawns;
		public string text;
	}

	public IEnumerator StartLevel(int level) {
		var levelConfiguration = levelConfigurations[level];

		enemiesInLevel = levelConfiguration.ghostIdxs.Count;
		foreach (int idx in levelConfiguration.ghostIdxs) {
			SpawnGhost (idx);
			yield return new WaitForSeconds (levelConfiguration.delayBetweenSpawns);
		}
	}

	public void DecreaseEnemy() {
		enemiesInLevel --;
		if(enemiesInLevel <=0) {
			if (onLevelEnd != null) {
				onLevelEnd ();
			}
		}
	}

	public void ReleaseAll() {
		foreach (Transform c in transform)
		{
			foreach (var po in c.GetComponent<PoolController.Pool>().poolObjects)
			{
				po.Release();
				po.gameObject.SetActive (false);
			}
		}
	}

}
