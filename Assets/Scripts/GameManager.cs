﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class GameManager : UnitySingleton<GameManager> {
	public int level;

	public bool isGameOver;

	void OnEnable() {
		GhostSpawner.onLevelEnd += PlayNextLevel;
	}

	void OnDisable() {
		GhostSpawner.onLevelEnd -= PlayNextLevel;
	}

	public void PlayNextLevel() {
		// Win Game Condition
		if (level == GhostSpawner.instance.levelConfigurations.Count) {
			DOVirtual.DelayedCall (4, ()=> {
				InGameMenuController.instance.ShowMenuWin (true);
			});
			return;
		}

		var nextLevel = GhostSpawner.instance.levelConfigurations [level];
		InGameMenuController.instance.ShowMenuLevel (true, nextLevel.text, level + 1);
		DOVirtual.DelayedCall (8, () => {
			InGameMenuController.instance.ShowMenuLevel (false);	
			StartCoroutine(GhostSpawner.instance.StartLevel (level++));			
		});
	}

	public void GameOver() {
		isGameOver = true;
		InGameMenuController.instance.ShowMenuLose (true);
	}

	public void Restart() {
		level = 0;
		isGameOver = false;
	}
		
}
