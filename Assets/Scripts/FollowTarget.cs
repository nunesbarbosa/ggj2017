using System;
using UnityEngine;



    public class FollowTarget : MonoBehaviour
    {
        public Transform target;
        public Vector3 offset = new Vector3(0f, 7.5f, 0f);


        private void LateUpdate()
        {
		var pos = target.position + offset;
		pos.y = transform.position.y;
		transform.position = pos;
        }
    }
