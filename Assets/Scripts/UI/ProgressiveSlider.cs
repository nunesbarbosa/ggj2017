﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ProgressiveSlider : Slider {

	private bool isOver;
	private float t;

	public override void OnPointerDown (UnityEngine.EventSystems.PointerEventData eventData)
	{
		base.OnPointerDown (eventData);	
		isOver = true;
	}


	public override void OnPointerExit (UnityEngine.EventSystems.PointerEventData eventData)
	{
		base.OnPointerExit (eventData);
		isOver = false;
	}

	void Update() {
		if (isOver && t < maxValue) {
			t += Time.deltaTime;
			value = Mathf.Lerp (0, maxValue, t);
		} else if(!isOver && t>0){
			t -= Time.deltaTime * 2;
			value = Mathf.Lerp (0, maxValue, t);
		}

		if (value >= maxValue) {
			interactable = false;
		}
	}
}
