﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

	public Slider sliderStart; 
	public AudioSource sliderFillAS;
	public AudioSource sliderCompleteAS;

	// Use this for initialization
	void Start () {
		sliderStart.onValueChanged.AddListener ((val)=>{
			if(!sliderFillAS.isPlaying) {
			sliderFillAS.Play();
			}
			if(val >= sliderStart.maxValue) {
				sliderFillAS.Stop();
				sliderCompleteAS.Play();
				GameManager.instance.Restart();
				SceneManager.LoadScene("Game");
			}
		});
	}

}
