﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class InGameMenuController : MonoBehaviour {

	public GameObject panelWin;
	public GameObject panelLose;
	public GameObject panelLevel;
	public Slider sliderRestartLose;
	public Slider sliderRestartWin;
	public Text textLevel;
	public Text textWave;

	public AudioSource sliderFillAS;
	public AudioSource sliderCompleteAS;

	private CanvasGroup canvasGroup;

	public static InGameMenuController instance;

	void Awake() {
		instance = this;
	}

	// Use this for initialization
	void Start () {
		canvasGroup = GetComponent<CanvasGroup> ();

		sliderRestartLose.onValueChanged.AddListener ((val)=>{
			if(!sliderFillAS.isPlaying) {
				sliderFillAS.Play();
			}
			if(val >= sliderRestartLose.maxValue) {
				sliderFillAS.Stop();
				sliderCompleteAS.Play();
				GhostSpawner.instance.ReleaseAll();
				SceneManager.LoadScene("Menu");
			}
		});

		sliderRestartWin.onValueChanged.AddListener ((val)=>{
			if(sliderFillAS) {
			sliderFillAS.Play();
			}
			if(val >= sliderRestartWin.maxValue) {
				sliderFillAS.Stop();
				sliderCompleteAS.Play();
				GhostSpawner.instance.ReleaseAll();
				SceneManager.LoadScene("Menu");
			}
		});

		panelWin.SetActive (false);
		panelLose.SetActive (false);
		panelLevel.SetActive (false);
	}

	public void ShowMenuWin(bool isShow) {
		if (isShow) {
			panelWin.SetActive (true);
			canvasGroup.DOFade (1, 1);
		}
		if (!isShow) {
			canvasGroup.DOFade (0, 1).OnComplete(()=>{
				panelWin.SetActive(false);
			});
		}
	}

	public void ShowMenuLose(bool isShow) {
		if (isShow) {
			panelLose.SetActive (true);
			canvasGroup.DOFade (1, 1);
		}
		if (!isShow) {
			canvasGroup.DOFade (0, 1).OnComplete(()=>{
				panelLose.SetActive(false);
			});
		}
	}

	public void ShowMenuLevel(bool isShow, string txt = "", int level = 0) {
		if (isShow) {
			panelLevel.SetActive (true);
			canvasGroup.DOFade (1, 1);
			textLevel.text = txt;
			textWave.text = "Onda " + level;
		}
		if (!isShow) {
			canvasGroup.DOFade (0, 1).OnComplete(()=>{
				panelLevel.SetActive(false);
			});
		}
	}

}
