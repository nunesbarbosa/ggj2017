﻿/***************************************************************************\
Project:      Crossy Template
Author:       Guilherme Nunes Barbosa (gnunesb@gmail.com)
\***************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
    The Game Controller is the main Class of the project.
    It is responsible for the main actions of the game
*/
namespace NiobiumStudios
{
    public class PoolController : UnitySingleton<PoolController>
    {
        private Dictionary<string, Pool> _pool;                 // Objects buffer pool

        override protected void Awake()
        {
            base.Awake();
            _pool = new Dictionary<string, Pool>();
        }

        [Serializable]
        public class PoolConfiguration 
        {
            public int poolSize;
            public GameObject prefab;

            public string GetID()
            {
                return prefab.name;
            }
        }

        public class Pool : MonoBehaviour
        {
            public int poolSize;
            public List<PoolObject> poolObjects;
            public GameObject prefab;

            public void Initialize()
            {
                poolObjects = new List<PoolObject>(poolSize);

                for (int i = 0; i < poolSize; i++)
                {
                    PoolObject po = CreateInstance();
                    poolObjects.Add(po);
                }
            }

            public string GetID()
            {
                return prefab.name;
            }

            private PoolObject CreateInstance()
            {
                GameObject go = GameObject.Instantiate(prefab, new Vector3(-1000, -1000, 0), Quaternion.identity) as GameObject;
                PoolObject po = go.AddComponent<PoolObject>();

                po.container = transform;
                go.transform.SetParent(transform);
                po.go = go;
                po.released = true;
                go.SetActive(false);

                return po;
            }

            public GameObject FindGameObject()
            {
                GameObject nextGo = null;
                foreach (PoolObject po in poolObjects)
                {
                    if (po.released)
                    {
                        nextGo = po.go;
                        po.released = false;
                        break;
                    }
                }

                if (nextGo == null)
                {
                    PoolObject po = CreateInstance();
                    poolObjects.Add(po);
                    poolSize++;
                    nextGo = po.go;
                    po.released = false;
                }

                nextGo.SetActive(true);

                return nextGo;
            }
        }

        public class PoolObject : MonoBehaviour
        {
            public bool released;
            public GameObject go;
            public Transform container;

            public void Release()
            {
                Transform coin = FindDeepChild(transform, "Coin");
                if (coin)
                {
                    Destroy(coin.gameObject);
                }

                released = true;
                go.transform.SetParent(container);
                go.transform.position = new Vector3(-10000, -10000, 0);
                go.SetActive(false);
            }
        }

        public void InitializePool(PoolConfiguration[] pools)
        {
            foreach (PoolConfiguration poolConfiguration in pools)
            {
                if (!_pool.ContainsKey(poolConfiguration.GetID())) {

                    Transform containerGo = new GameObject("Pool - " + poolConfiguration.GetID()).transform;
                    containerGo.SetParent(PoolController.instance.transform);

                    Pool newPool = containerGo.gameObject.AddComponent<Pool>();
                    newPool.prefab = poolConfiguration.prefab;
                    newPool.poolSize = poolConfiguration.poolSize;

                    newPool.Initialize();
                    _pool.Add(newPool.GetID(), newPool);
                }
            }
        }

        public GameObject GetFromPool(string name)
        {
            return _pool[name].FindGameObject();
        }

        public static Transform FindDeepChild(Transform aParent, string aName)
        {
            var result = aParent.Find(aName);
            if (result != null)
                return result;
            foreach (Transform child in aParent)
            {
                result = FindDeepChild(child, aName);
                if (result != null)
                    return result;
            }
            return null;
        }
    }
}