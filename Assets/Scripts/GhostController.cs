﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using NiobiumStudios;

public class GhostController : MonoBehaviour {

	public Renderer[] ghostRenderers;
	private List<Material> ghostMaterials = new List<Material>();
	public float maxTransparencyTime = 0.5f;
	public float transparencyValue = 0.1f;
	public float speed = 1;

	private bool isTransparent;
	public float health;
	public float maxHealth = 10f;
	private bool isOver;
	public GvrAudioSource gameOverAS;
	public GvrAudioSource dieAS;

	private void Awake()
	{
		health = maxHealth;

		foreach (var rend in ghostRenderers) {
			ghostMaterials.AddRange (rend.materials);
		}
	}

	void Update() {
		if (health <= 0 || GameManager.instance.isGameOver) {
			return;
		}
		transform.position = Vector3.MoveTowards (transform.position, CharacterController.instance.playerFace.position, speed * Time.deltaTime);
		transform.LookAt (CharacterController.instance.transform.position);

		if (!isTransparent) {
			isTransparent = true;
			foreach (var mat in ghostMaterials) {
				mat.DOFade (transparencyValue, maxTransparencyTime * 2).SetEase(Ease.InSine);
			}
		}

		if (isOver) {
			health -= Time.deltaTime;
		} else if(health< maxHealth){
			health += Time.deltaTime;
		}

		if (health <= 0) {
			CharacterController.instance.beanAS.Stop ();
			dieAS.Play ();

			foreach (var mat in ghostMaterials) {
				var c = mat.color;
				c.a = 1f;
				mat.color = c;
			}
			FadeOut (dieAS.clip.length);
			transform.DOMoveY (transform.position.y + 10, dieAS.clip.length);

			StartCoroutine (ReleaseGhost (dieAS.clip.length));

			GetComponent<Collider> ().enabled = false;
			GhostSpawner.instance.DecreaseEnemy ();

		}
	}

	void Start() {
		foreach (var ghostRenderer in ghostRenderers) {
			var materials = ghostRenderer.materials;
		}

		FadeOut (0f);
	}

	private IEnumerator ReleaseGhost(float delay) {
		yield return new WaitForSeconds (delay);

		GetComponent<PoolController.PoolObject> ().Release ();
	}

	public void Reset() {
		GetComponent<Collider> ().enabled = true;
		FadeOut (0f);
		health = maxHealth;
		isOver = false;
	}

	public void OnPointerDown() {
		isOver = true;
		CharacterController.instance.beanAS.Play ();
	}

	public void OnPointerExit() {
		isOver = false;
		CharacterController.instance.beanAS.Stop ();
	}

	void OnTriggerEnter(Collider col) {
		if (col.CompareTag ("Diapasao")) {
			FadeIn (maxTransparencyTime);
		}

		if (col.CompareTag ("Player") && !GameManager.instance.isGameOver) {
			GameManager.instance.GameOver ();
			gameOverAS.Play ();
			print("Game Over");
		}
	}

	private void FadeIn(float fadeTime) {
		foreach (var mat in ghostMaterials) {
			mat.DOFade (1, fadeTime).OnComplete (() => {
				isTransparent = false;
			});
		}
	}

	private void FadeOut(float time) {
		foreach (var mat in ghostMaterials) {
			mat.DOFade (time, transparencyValue).SetEase(Ease.OutSine).OnComplete (() => {
				isTransparent = false;
			});
		}
	}
		
}