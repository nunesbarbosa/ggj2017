﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

[ExecuteInEditMode]
public class LookAtCamera : MonoBehaviour
{
    private Camera cameraToLookAt;

    void Start()
    {
        cameraToLookAt = Camera.main;
    }

    void LateUpdate()
    {
        //Vector3 v = cameraToLookAt.transform.position - transform.position;
        //v.x = v.z = 0.0f;
        //transform.rotation = Quaternion.LookRotation(transform.position - cameraToLookAt.transform.position);
        //transform.LookAt(cameraToLookAt.transform.position - v);

        transform.LookAt(cameraToLookAt.transform);
        transform.Rotate(0, 180, 0);
    }
}
